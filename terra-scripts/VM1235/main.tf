resource "azurerm_resource_group" "myterraformgroup" {
   name="${var.resource_group_name}" 
   location="${var.location}"
   tags {
      environment="${var.env}" 
   }
}
resource "azurerm_virtual_network" "myterraformnetwork" {
   name="${var.virtual_network_name}"
   address_space="${var.address_space}"
   location="${var.location}"
   resource_group_name="${azurerm_resource_group.myterraformgroup.name}"
      tags {
         environment="${var.env}" 
      }
}
resource "azurerm_subnet" "myterraformsubnet"{
   name="${var.subnet_name}" 
   resource_group_name="${azurerm_resource_group.myterraformgroup.name}"
   virtual_network_name="${azurerm_virtual_network.myterraformnetwork.name}"
   address_prefix="${var.add_pref}"
}

### 3 MACHINES DE LA MEME REGION
resource "azurerm_public_ip" "myterraformpublicip" {
   name="${var.public_ip_name}"#-${count.index + 1}
   location="${var.location}"
   resource_group_name="${azurerm_resource_group.myterraformgroup.name}"
   public_ip_address_allocation="${var.type_allocation_address}"
   #count="${length(var.ip_addresses)}"
   tags {
      environment="${var.env}"
}
}

resource "azurerm_network_security_group" "myterraformnsg" {
   name="${var.security_group_name}"
   location="${var.location}"
   resource_group_name="${azurerm_resource_group.myterraformgroup.name}"

   tags {
      environment="${var.env}" 
   }

security_rule {
   name="${var.protocol_name1}"
   priority="${var.priority}"
   direction="${var.direction}" 
   access="${var.allow}"
   protocol="${var.tcp}" 
   source_port_range="*"
   destination_port_range="${var.destination_port}" 
   source_address_prefix="*"
   destination_address_prefix="*"
}
security_rule {
   name="${var.protocol_name2}"
   priority="${var.priority2}"
   direction="${var.direction}"
   access="${var.allow}"
   protocol="${var.tcp}"
   source_port_range="*"
   destination_port_range="${var.destination_port2}"
   source_address_prefix="*"
   destination_address_prefix="*"
}
}

resource "azurerm_network_interface" "myterraformnic" {
   name="${var.network_interface_name}-${count.index+1}"
   location="${var.location}"
   resource_group_name="${azurerm_resource_group.myterraformgroup.name}"
   count="${length(var.ip_addresses)}"
   
   ip_configuration {
      name="${var.ip_conf_name}"
      subnet_id="${azurerm_subnet.myterraformsubnet.id}"
      private_ip_address_allocation="${var.type_allocation_address}"
      #public_ip_address_id="${azurerm_public_ip.myterraformpublicip.*.id[count.index]}"
      public_ip_address_id="${""}"
      private_ip_address="${element(var.ip_addresses,count.index)}"

   }
   network_security_group_id="${azurerm_network_security_group.myterraformnsg.id}"
   #output "interface reseau id" {
   #   value="${azurerm_network_interface.myterraformnic.id}"
   #}


   tags {
      environment="${var.env}" 
   }
}
resource "random_id" "randomId"{
	keepers={
	 # Generate a new ID only when a new resource group is defined
 
	resource_group="${azurerm_resource_group.myterraformgroup.name}"
         }
        byte_length="${var.byte_length}"
}
resource "azurerm_storage_account" "mystorageaccount" { 
 	name="diag${random_id.randomId.hex}"
	resource_group_name="${azurerm_resource_group.myterraformgroup.name}"
	location="${var.location}"
	account_replication_type="${var.account_replication_type}"
	account_tier="${var.account_tiers}"
 	tags {
      environment="${var.env}" 
   }
}
resource "azurerm_virtual_machine" "myterraformvm"{
	name="${var.vm_name}-${count.index+1}"
	location="${var.location}"
	resource_group_name="${azurerm_resource_group.myterraformgroup.name}"	
        network_interface_ids=["${azurerm_network_interface.myterraformnic.*.id[count.index]}"]
	vm_size="${var.vm_size}"
	count="${length(var.ip_addresses)}"

	storage_os_disk{
		name="${var.os_disk_name}-${count.index + 1}"
		caching="${var.catching}"
		create_option="${var.create_option}"
		managed_disk_type="${var.managed_disk_type}"
                
}
storage_image_reference{
	publisher="${var.publisher}"
	offer="${var.offer}"
	sku="${var.sku}"
	version="${var.version}"
}
os_profile{
	computer_name="${var.computer_name}"
	admin_username="${var.admin_username}"
	}
os_profile_linux_config{
	disable_password_authentication="${var.enabled}"
	ssh_keys{
		path="/home/azureuser/.ssh/authorized_keys"
		key_data="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDIG9ZWtfmEUgtHO1i1jhG5Q1ZKMLr2wB1JY2niYKfpEDqm3oWvq11uhOMiNF48rh4OluHWb0Nz0q5R9eUbAO5gQMVerdJ/gSueAquCprhQ9KUEmrgoziRoOuZlVJQZE6fuUsEVf0JL2L1glyDZcGc5zpkYSZ6zQuI0blLMh8Ar4yuzxJSQfLv2xqa6b5C0O5sFWPQFJb0OnuBmp577h+daogyJC7fnfpFnbX2o6qPfXK0/6jOa0xoM9d2SjvJORRwrcDqPFd6roldVnnq7oKtjfX46UERerR3jW8CYrlQVPSzxFuAHm+s6iKYZO7K+rvdoS9s6cuIlnC+Yt2TejOCf root@CentOs.formation"
	}
}
boot_diagnostics{
	enabled="${var.enabled}"
	storage_uri="${azurerm_storage_account.mystorageaccount.primary_blob_endpoint}"
}
tags{
environment="${var.env}" 
	}
}
#### UNE 4° MACHINE

resource "azurerm_virtual_network" "myterraformnetwork5" {
   name="${var.virtual_network_name2}"
   address_space="${var.address_space}"
   location="${var.location2}"
   resource_group_name="${azurerm_resource_group.myterraformgroup.name}"
      tags {
         environment="${var.env}" 
      }
}
resource "azurerm_subnet" "myterraformsubnet5"{
   name="${var.subnet_name}" 
   resource_group_name="${azurerm_resource_group.myterraformgroup.name}"
   virtual_network_name="${azurerm_virtual_network.myterraformnetwork5.name}"
   address_prefix="${var.add_pref}"
}
resource "azurerm_public_ip" "myterraformpublicip5" {
   name="${var.public_ip_name2}"#-${count.index + 1}
   location="${var.location2}"
   resource_group_name="${azurerm_resource_group.myterraformgroup.name}"
   public_ip_address_allocation="${var.type_allocation_address}"
   #count="${length(var.ip_addresses)}"
   tags {
      environment="${var.env}"
}
}

resource "azurerm_network_security_group" "myterraformnsg5" {
   name="${var.security_group_name}5"
   location="${var.location2}"
   resource_group_name="${azurerm_resource_group.myterraformgroup.name}"

   tags {
      environment="${var.env}" 
   }

security_rule {
   name="${var.protocol_name1}"
   priority="${var.priority}"
   direction="${var.direction}" 
   access="${var.allow}"
   protocol="${var.tcp}" 
   source_port_range="*"
   destination_port_range="${var.destination_port}" 
   source_address_prefix="*"
   destination_address_prefix="*"
}
security_rule {
   name="${var.protocol_name2}"
   priority="${var.priority2}"
   direction="${var.direction}"
   access="${var.allow}"
   protocol="${var.tcp}"
   source_port_range="*"
   destination_port_range="${var.destination_port2}"
   source_address_prefix="*"
   destination_address_prefix="*"
}
}

resource "azurerm_network_interface" "myterraformnic5" {
   name="${var.network_interface_name2}"
   location="${var.location2}"
   resource_group_name="${azurerm_resource_group.myterraformgroup.name}"
   
   ip_configuration {
      name="${var.ip_conf_name}"
      subnet_id="${azurerm_subnet.myterraformsubnet5.id}"
      private_ip_address_allocation="${var.type_allocation_address}"
      public_ip_address_id="${azurerm_public_ip.myterraformpublicip5.id}"
      private_ip_address="${element(var.ip_addresses,4)}"

   }
   network_security_group_id="${azurerm_network_security_group.myterraformnsg5.id}"
   #output "interface reseau id" {
   #   value="${azurerm_network_interface.myterraformnic5.id}"
   #}


   tags {
      environment="${var.env}" 
   }
}
#resource "random_id" "randomId"{
#	keepers={
	 # Generate a new ID only when a new resource group is defined
 
#	resource_group="${azurerm_resource_group.myterraformgroup.name}"
#         }
#        byte_length="${var.byte_length}"
#}
resource "azurerm_storage_account" "mystorageaccount5" { 
 	name="diag${random_id.randomId.hex}"
	resource_group_name="${azurerm_resource_group.myterraformgroup.name}"
	location="${var.location}"
	account_replication_type="${var.account_replication_type}"
	account_tier="${var.account_tiers}"
 	tags {
      environment="${var.env}" 
   }
}
resource "azurerm_virtual_machine" "myterraformvm5"{
	name="${var.vm_name2}"
	location="${var.location2}"
	resource_group_name="${azurerm_resource_group.myterraformgroup.name}"	
        network_interface_ids=["${azurerm_network_interface.myterraformnic5.id}"]
	vm_size="${var.vm_size}"

	storage_os_disk{
		name="${var.os_disk_name2}"
		caching="${var.catching}"
		create_option="${var.create_option}"
		managed_disk_type="${var.managed_disk_type}"
                
}
storage_image_reference{
	publisher="${var.publisher}"
	offer="${var.offer}"
	sku="${var.sku}"
	version="${var.version}"
}
os_profile{
	computer_name="${var.computer_name2}"
	admin_username="${var.admin_username}"
	}
os_profile_linux_config{
	disable_password_authentication="${var.enabled}"
	ssh_keys{
		path="/home/azureuser/.ssh/authorized_keys"
		key_data="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDIG9ZWtfmEUgtHO1i1jhG5Q1ZKMLr2wB1JY2niYKfpEDqm3oWvq11uhOMiNF48rh4OluHWb0Nz0q5R9eUbAO5gQMVerdJ/gSueAquCprhQ9KUEmrgoziRoOuZlVJQZE6fuUsEVf0JL2L1glyDZcGc5zpkYSZ6zQuI0blLMh8Ar4yuzxJSQfLv2xqa6b5C0O5sFWPQFJb0OnuBmp577h+daogyJC7fnfpFnbX2o6qPfXK0/6jOa0xoM9d2SjvJORRwrcDqPFd6roldVnnq7oKtjfX46UERerR3jW8CYrlQVPSzxFuAHm+s6iKYZO7K+rvdoS9s6cuIlnC+Yt2TejOCf root@CentOs.formation"
	}
}
boot_diagnostics{
	enabled="${var.enabled}"
	storage_uri="${azurerm_storage_account.mystorageaccount.primary_blob_endpoint}"
}
tags{
environment="${var.env}" 
	}
}
